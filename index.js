require('dotenv').config();
var express = require("express");
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var app = express();
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.use(session({secret: 'secret', 
                 resave: false, 
                 saveUninitialized: true,}));


var passport = require('passport');
var saml = require('passport-saml');
var fs = require('fs');

passport.serializeUser(function(user, done) {
    console.log('-----------------------------');
    console.log('serialize user');
    console.log(user);
    console.log('-----------------------------');
    done(null, user);
});
passport.deserializeUser(function(user, done) {
    console.log('-----------------------------');
    console.log('deserialize user');
    console.log(user);
    console.log('-----------------------------');
    done(null, user);
});

// https://medium.com/disney-streaming/setup-a-single-sign-on-saml-test-environment-with-docker-and-nodejs-c53fc1a984c9

// var samlStrategy = new saml.Strategy({
//     callbackUrl: process.env.CALLBACK_URL,
//     entryPoint: 'https://marlon-joseph-dev.onelogin.com/trust/saml2/http-post/sso/c0a518da-fd2a-4929-8ec7-ff28d387bdc8',
//     issuer: 'mj-saml-poc',
//     audience:'mj-saml-audience',
//     identifierFormat: null,
//     decryptionPvk: fs.readFileSync(__dirname + '/certs/key.pem', 'utf8'),
//     privateKey: fs.readFileSync(__dirname + '/certs/key.pem', 'utf8'),
//     disableRequestedAuthnContext: true,
//   }, function(profile, done) {
//       return done(null, profile);
// });

var samlStrategy = new saml.Strategy({
    callbackUrl: process.env.CALLBACK_URL,
    entryPoint: 'http://localhost:8080/auth/realms/demo/protocol/saml',
    issuer: 'mj-saml-poc',
    //audience:'mj-saml-audience',
    identifierFormat: null,
    decryptionPvk: fs.readFileSync(__dirname + '/certs/key.pem', 'utf8'),
    privateKey: fs.readFileSync(__dirname + '/certs/key.pem', 'utf8'),
    disableRequestedAuthnContext: true,
  }, function(profile, done) {
      return done(null, profile);
});

passport.use('samlStrategy', samlStrategy);
app.use(passport.initialize({}));
app.use(passport.session({}));

app.get('/metadata',
    function(req, res) {
        res.type('application/xml'); 
        res.status(200).send(
          samlStrategy.generateServiceProviderMetadata(
             fs.readFileSync(__dirname + '/certs/cert.pem', 'utf8'), 
             fs.readFileSync(__dirname + '/certs/cert.pem', 'utf8')
          )
        );
    }
);

app.get('/login',
    function (req, res, next) {
        console.log('-----------------------------');
        console.log('/Start login handler');
        next();
    },
    passport.authenticate('samlStrategy'),
);

app.post('/login/callback',
    function (req, res, next) {
        console.log('-----------------------------');
        console.log('/Start login callback ');
        next();
    },
    passport.authenticate('samlStrategy'),
    function (req, res) {
        console.log('-----------------------------');
        console.log('login call back dumps');
        console.log(req.user);
        console.log('-----------------------------');
        res.send('Log in Callback Success');
    }
);

app.get('/',
    function(req, res) {
        res.send('Test Home Page');
    }
);

var server = app.listen(4300, function () {
    console.log('Listening on port %d', server.address().port)
});